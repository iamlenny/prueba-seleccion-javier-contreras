import os
from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys
import time
import logging
from dotenv import dotenv_values

import pandas as pd
import numpy as np

logging.basicConfig(filename='data_scraping.log', level=logging.DEBUG)

config = dotenv_values('.env')

# ChromeDriver PATH
#PATH = './driver/chromedriver.exe'
# URL de pruebas - DATOS ABIERTOS
# URL = 'https://www.datosabiertos.gob.pe/'

# PARTE 1 PRUEBA TEAMCORE - DESCARGAR ARCHIVO .csv
def download_file(PATH, URL):
    service = Service(PATH)

    options = webdriver.ChromeOptions()
    prefs = {"download.default_directory": os.path.dirname(os.path.abspath(__file__))}
    options.add_experimental_option("prefs", prefs)
    options.add_experimental_option('excludeSwitches', ['enable-logging'])

    driver = webdriver.Chrome(service=service, options=options)
    driver.maximize_window()

    try:
        driver.get(URL)

        # Filtro por tipo de contenido
        tipo_contenido = WebDriverWait(driver, 10).until(
            EC.presence_of_element_located((By.ID, 'facetapi-link--199'))
        )

        tipo_contenido.click()
        time.sleep(3)

        # Filtro por categoria
        categoria = WebDriverWait(driver, 10).until(
            EC.presence_of_element_located((By.ID, 'facetapi-link'))
        )

        categoria.click()
        time.sleep(3)

        # Desplegar la información del formato

        select_options = WebDriverWait(driver, 10).until(
            EC.presence_of_element_located((By.XPATH, '/html/body/div[3]/div/div/section/div/div/div/div/div[1]/div/div[4]/h2'))
        )

        select_options.click()
        time.sleep(1)

        # Filtro por formato
        formato = WebDriverWait(driver, 10).until(
            EC.presence_of_element_located((By.PARTIAL_LINK_TEXT, 'csv'))
        )

        formato.click()
        time.sleep(3)

        # Buscar el reporte por "donaciones"
        nombre_reporte = WebDriverWait(driver, 10).until(
            EC.presence_of_element_located((By.NAME, 'query'))
        )

        nombre_reporte.send_keys('donaciones')
        time.sleep(0.1)
        nombre_reporte.send_keys(Keys.RETURN)    

        # Enviar la información
        consultar = WebDriverWait(driver, 10).until(
            EC.presence_of_element_located((By.ID, 'edit-submit-dkan-datasets'))
        ).click()

        time.sleep(3)

        # Ingresar a la data
        data_btn = WebDriverWait(driver, 10).until(
            EC.presence_of_element_located((By.LINK_TEXT, 'data'))
        )

        data_btn.click()

        time.sleep(3)

        # Descargar archivo
        download_btn = WebDriverWait(driver, 10).until(
            EC.presence_of_element_located((By.XPATH, '//*[@id="data-and-resources"]/div/div/ul/li[1]/div/span/a[2]'))
        )

        download_btn.click()
        # Espera para conexiones de red con baja velocidad - testeo con velocidad de descarga de 250 kbps
        time.sleep(5)
    except Exception as e:
        logging.error(e)
    finally:
        driver.quit()


# PARTE 2 - FILTRAR EL ARCHIVO .csv OBTENIDO POR LA PÁGINA DE DATOSABIERTOS
def pandas_filter(filename, filter):
    data = pd.read_csv(filename, index_col=0, encoding='latin-1', sep='|', on_bad_lines='skip')
    df = pd.DataFrame(data)
    new_data = df.replace(np.nan, '0')
    # Filtro por sector (11 = SALUD)
    new_data = new_data.loc[new_data['SECTOR'] == 11]
    # data to csv
    new_data.reset_index().to_csv(f"{str(filter)}.csv", header=True, index=False, sep='\t')


# Ejecución de código
if __name__ == '__main__':
    download_file(config['CHROMEDRIVER_PATH'], config['URL'])
    pandas_filter('pcm_donaciones.csv', 11)