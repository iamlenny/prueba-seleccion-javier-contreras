# Test Backend Developer - 2022  - Season 1 (Junior)

### Requisitos

- Python
- pip
- ChromeDriver (Se encuentra en la raíz del proyecto)

### Instalación de librerías

Las librerías necesarias se encuentran en el archivo "requirements.txt"

```
pip install -r requirements.txt
```

### Variables de entorno

Estas variables de entorno se encuentran en el archivo .env ubicado en la raíz del proyecto

- CHROMEDRIVER_PATH: URL del ChromeDriver
- URL: URL de la página de DATOS ABIERTOS

### Ejecutar el proyecto

```
python main.py
```